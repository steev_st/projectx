<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 2/3/15
 * Time: 1:22 PM
 */

namespace System\Core;

use System\Core\Config\Paths;
use System\Core\Config\Files;

class Loader {

    const CLASS_LOAD_FUNCTION_NAME  = 'loadClass';

    const NAMESPACE_CLASS           = 'Classes';
    const NAMESPACE_CONTROLLER      = 'Controller';
    const NAMESPACE_LIBRARY         = 'Library';
    const NAMESPACE_VIEW            = 'View';

    private $aNamespaceDir          = array();

    public function __construct()
    {
        $this->initializeSplAutoLoadRegister();
        $this->initializeNamespaceDirRelation();

    }

    private function initializeSplAutoLoadRegister()
    {
        spl_autoload_register(
            array(
                $this,
                self::CLASS_LOAD_FUNCTION_NAME
            )
        );
    }

    private function initializeNamespaceDirRelation()
    {
        $this->aNamespaceDir[self::NAMESPACE_CLASS]         = Paths::getClassesPath();
        $this->aNamespaceDir[self::NAMESPACE_CONTROLLER]    = Paths::getControllersPath();
        $this->aNamespaceDir[self::NAMESPACE_LIBRARY]       = Paths::getLibrariesPath();
        $this->aNamespaceDir[self::NAMESPACE_VIEW]          = Paths::getViewsPath();
    }

    private function getDirByNamespaceFirstSegment($sSegment)
    {
        return $this->aNamespaceDir[$sSegment];
    }

    public function loadClass($sClassName)
    {
        if (substr($sClassName, 0, 1) == '\\') {
            $sClassName = substr($sClassName, 1, strlen($sClassName));
        }

        $iMainNamespaceIndex = 1;
        $iRealClassNameIndex = 1;

        $aNamespaceSegments     = explode('\\', $sClassName);
        $iRealClassNameIndex    = count($aNamespaceSegments);
        $sRealClassName         = end($aNamespaceSegments);
        $sMainNamespace         = reset($aNamespaceSegments);

        $iMainNamespaceIndex--;
        $iRealClassNameIndex--;

        foreach (Files::getPossibleFileExtensions() as $sFileExtension) {
            $aNamespaceSegments = array_replace(
                $aNamespaceSegments,
                array(
                    $iMainNamespaceIndex => $this->getDirByNamespaceFirstSegment($sMainNamespace),
                    $iRealClassNameIndex => $sRealClassName.$sFileExtension
                )
            );

            $sFilePath = implode(
                Paths::DS,
                $aNamespaceSegments
            );

            if (file_exists($sFilePath)) {
                try {
                    include_once($sFilePath);
                } catch(\Exception $e) {
                    throw new \Exception('System can\'t load class '.$sClassName);
                }
            }
        }
    }
} 