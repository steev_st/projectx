<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 1/31/15
 * Time: 2:58 PM
 */

namespace System\Core;


class Singleton
{
    /**
     * Returns the *Singleton* instance of this class.
     *
     * @staticvar Singleton $instance The *Singleton* instances of this class.
     *
     * @return Singleton The *Singleton* instance.
     */
    public static function getInstance()
    {
        static $instance = null;
        if (null === $instance) {
            $instance = new static();
        }

        return $instance;
    }
}