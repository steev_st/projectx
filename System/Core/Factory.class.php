<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 2/3/15
 * Time: 8:54 PM
 */

namespace System\Core;

use System\Core\Core;
use System\Core\Loader;
use System\Core\Router;

class Factory {
    public static function initializeCore()
    {
        new Core(
            new Loader(),
            new Router()
        );
    }
} 