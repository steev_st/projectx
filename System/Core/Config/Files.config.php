<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 2/14/15
 * Time: 9:29 PM
 */

namespace System\Core\Config;

class Files {
    const FILE_EXTENSION_CLASS = '.class.php';
    const FILE_EXTENSION_INCLUDE = '.include.inc';
    const FILE_EXTENSION_CONFIG = '.config.php';
    const FILE_EXTENSION_CONTROLLER = '.controller.php';

    public static function getPossibleFileExtensions()
    {
        return array(
            self::FILE_EXTENSION_CLASS,
            self::FILE_EXTENSION_INCLUDE,
            self::FILE_EXTENSION_CLASS,
            self::FILE_EXTENSION_CONTROLLER
        );
    }
}