<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 2/3/15
 * Time: 11:32 AM
 */

namespace System\Core\Config;

class Paths {

    const DS                    = DIRECTORY_SEPARATOR;
    const DIR_NAME_PROJECT      = 'projectx.com';
    const DIR_NAME_APP          = 'Application';
    const DIR_NAME_CLASSES      = 'Classes';
    const DIR_NAME_CONTROLLERS  = 'Controllers';
    const DIR_NAME_VIEWS        = 'Views';
    const DIR_NAME_LIBRARIES    = 'Libraries';

    const DIR_NAME_SYSTEM       = 'System';
    const DIR_NAME_CORE         = 'Core';

    private static function getMainDirPath()
    {
        $aPathSegments = explode(self::DS, __DIR__);

        if (!in_array(self::DIR_NAME_PROJECT, $aPathSegments)) {
            throw new \Exception('Please define correct constant DIR_NAME_PROJECT in '.__CLASS__);
        }

        $bAddToPath     = true;
        $aMainDirPath   = array();

        foreach ($aPathSegments as $sSegment) {
            if (!$bAddToPath) {
                break;
            }

            $aMainDirPath[] = $sSegment;

            if ($sSegment == self::DIR_NAME_PROJECT) {
                $bAddToPath = false;
            }
        }

        return implode(self::DS, $aMainDirPath);
    }

    public static function getAppPath()
    {
        return implode(
            self::DS,
            array(
                self::getMainDirPath(),
                self::DIR_NAME_APP
            )
        );
    }

    public static function getSystemPath()
    {
        return implode(
            self::DS,
            array(
                self::getMainDirPath(),
                self::DIR_NAME_SYSTEM
            )
        );
    }

    public static function getCorePath()
    {
        return implode(
            self::DS,
            array(
                self::getMainDirPath(),
                self::DIR_NAME_SYSTEM,
                self::DIR_NAME_CORE
            )
        );
    }

    public static function getClassesPath()
    {
        return implode(
            self::DS,
            array(
                self::getMainDirPath(),
                self::DIR_NAME_APP,
                self::DIR_NAME_CLASSES
            )
        );
    }

    public static function getControllersPath()
    {
        return implode(
            self::DS,
            array(
                self::getMainDirPath(),
                self::DIR_NAME_APP,
                self::DIR_NAME_CONTROLLERS
            )
        );
    }

    public static function getViewsPath()
    {
        return implode(
            self::DS,
            array(
                self::getMainDirPath(),
                self::DIR_NAME_APP,
                self::DIR_NAME_VIEWS
            )
        );
    }

    public static function getLibrariesPath()
    {
        return implode(
            self::DS,
            array(
                self::getMainDirPath(),
                self::DIR_NAME_APP,
                self::DIR_NAME_LIBRARIES
            )
        );
    }

} 