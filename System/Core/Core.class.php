<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 2/3/15
 * Time: 1:08 PM
 */

namespace System\Core;

use System\Core\Config\Paths;
use System\Core\Loader;
use System\Core\Router;

class Core {
    /**
     * @return \System\Core\Loader
     */
    public function getLoader()
    {
        return $this->oLoader;
    }

    /**
     * @return \System\Core\Router
     */
    public function getRouter()
    {
        return $this->oRouter;
    }

    /**
     * @var Loader
     */
    private $oLoader;
    /**
     * @var Router
     */
    private $oRouter;

    public function __construct(Loader $oLoader, Router $oRouter)
    {
        $this->oLoader = $oLoader;
        $this->oRouter = $oRouter;
    }
}

