<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 2/4/15
 * Time: 8:51 PM
 */

namespace System\Core;

use \System\Core\Url;

class Router {
    /**
     * @var \System\Core\Url
     */
    private $Url;

    private $Controller;
    private $Method;
    private $Params;

    public function __construct()
    {
        $this->Url = new Url();
        $this->initialize();
    }

    private function initialize()
    {
        $Namespace = str_replace('{Class}',$this->Url->getController(), Loader::NAMESPACE_CONTROLLER.'\{Class}');
        if (!class_exists($Namespace)) {
            throw new \Exception('Controller with name "'.$this->Url->getController().'" does not exists!');
        }

        $this->Controller   = new $Namespace;
        $this->Method       = $this->Url->getMethod();
        $this->Params       = $this->Url->getParams();

        if (!method_exists($this->Controller, $this->Method)) {
            throw new \Exception('Method "'.$this->Url->getMethod().'" not found in controller "'.$this->Url->getController().'" !');
        }


        call_user_func(array($this->Controller, $this->Method), $this->Params);
    }



} 