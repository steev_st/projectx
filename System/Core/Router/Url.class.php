<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 2/14/15
 * Time: 10:03 PM
 */

namespace System\Core;


class Url {

    const URL_SEGMENTS_INDEX_CONTROLLER = 0;
    const URL_SEGMENTS_INDEX_METHOD     = 1;
    const URL_SEGMENTS_INDEX_PARAM      = 2;
    const URL_SEGMENTS_DEFAULT_INDEX    = '/index.php';

    const URL_SPECIAL_SEGMENT_PAGE      = 'page_-';
    const URL_SPECIAL_SEGMENT_OFFSET    = 'offset_-';
    const URL_SPECIAL_SEGMENT_LIMIT     = 'limit_-';


    /**
     * @var array
     */
    private $Segments;
    private $Url = '';
    private $Controller = 'Home';
    private $Method = 'Index';
    private $Params;
    private $SpecialParams = array();

    public static function getSpecialParams()
    {
        return array(
            self::URL_SPECIAL_SEGMENT_LIMIT,
            self::URL_SPECIAL_SEGMENT_OFFSET,
            self::URL_SPECIAL_SEGMENT_PAGE,
        );
    }


    public function __construct()
    {
        $this->initializeParams();
    }

    private function initializeParams()
    {
        $this->Url = str_replace(self::URL_SEGMENTS_DEFAULT_INDEX, '', $_SERVER['PHP_SELF']);

        if ($_SERVER['PHP_SELF'] != self::URL_SEGMENTS_DEFAULT_INDEX) {
            $this->Url = substr($this->Url, 1);
        }

        if ($this->Url === '') {
            return;
        }

        $this->Segments = explode('/', $this->Url);
        switch (count($this->Segments)) {
            case 1:
                $this->Controller = (string) $this->Segments[self::URL_SEGMENTS_INDEX_CONTROLLER];
                break;
            case 2:
                $this->Controller = (string) $this->Segments[self::URL_SEGMENTS_INDEX_CONTROLLER];
                $this->Method = (string) $this->Segments[self::URL_SEGMENTS_INDEX_METHOD];
                break;

            default:
                $this->selectSpecialParams();

                $this->Controller = (string) $this->Segments[self::URL_SEGMENTS_INDEX_CONTROLLER];
                $this->Method = (string) $this->Segments[self::URL_SEGMENTS_INDEX_METHOD];
                $this->Params = array_slice($this->Segments, self::URL_SEGMENTS_INDEX_METHOD + 1);

                break;
        }

//        $this->debug();



    }

    /**
     * @return array
     */
    public function getSegments()
    {
        return $this->Segments;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->Url;
    }

    /**
     * @return string
     */
    public function getController()
    {
        return $this->Controller;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->Method;
    }


    public function getParams()
    {
        return $this->Params;
    }

    private function _selectSpecialParams()
    {
        if (count($this->Segments) > 2) {
            $bIsSpecialValue = false;
            $sSpecialSegment = false;
            foreach ($this->Segments as $iKey => $sSegment) {

                if ($bIsSpecialValue) {
                    $this->SpecialParams[$sSpecialSegment] = $sSegment;
                    unset($this->Segments[$iKey]);
                    $bIsSpecialValue = false;
                }

                if (in_array($sSegment, self::getSpecialParams())) {
                    if (isset($this->SpecialParams[$sSegment])) {
                        unset($this->Segments[$iKey]);
                        unset($this->Segments[$iKey+1]);
                        continue;
                    }
                    $this->SpecialParams[$sSegment] = null;
                    $bIsSpecialValue = true;
                    $sSpecialSegment = $sSegment;
                    unset($this->Segments[$iKey]);
                }
            }
        }
    }
    private function selectSpecialParams()
    {
        if (count($this->Segments) > 2) {

            foreach ($this->Segments as $iKey => $sSegment) {
                $bIsSpecial = false;
                foreach (self::getSpecialParams() as $iKey2 => $sParamKey) {
                    if (strpos($sSegment, $sParamKey) !== false) {
                        $bIsSpecial = true;
                    }
                }

                if ($bIsSpecial) {
                    list($sKey, $mValue) = explode('_-', $sSegment);
                    if (!array_key_exists($sKey, $this->SpecialParams)) {
                        $this->SpecialParams[$sKey] = $mValue;
                    }
                    unset($this->Segments[$iKey]);
                }
            }
        }
    }

    public function debug()
    {
        var_dump($this->Controller);
        var_dump($this->Method);
        var_dump($this->Params);
        var_dump($this->SpecialParams);
        exit;
    }
} 